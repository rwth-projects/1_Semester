package de.schmidt.stuckat;

/**
 * Created by Marc Schmidt on 05.11.2016.
 */
public class StuckAt {

	private static final String[] COMBINATIONS = {"0000", "0001", "0010", "0011", "0100", "0101", "0110", "0111", "1000", "1001", "1010", "1011",
			"1100", "1101", "1110", "1111"};
	//	private static final String[] COMBINATIONS = {"000", "001", "010", "011", "100", "101", "110", "111"};
	private static final char TRUE = '1';

	public static void main(String[] args) {
		run();
	}

	private static void run() {
		for (String comb : COMBINATIONS) {
			boolean x0 = comb.charAt(0) == TRUE;
			boolean x1 = comb.charAt(1) == TRUE;
			boolean x2 = comb.charAt(2) == TRUE;
			boolean x3 = comb.charAt(3) == TRUE;
			//			boolean x3 = false;
			boolean expr = expression(x0, x1, x2, x3);
			boolean stuckAtOne = stuckAtOne(x0, x1, x2, x3);

			System.out.println(comb + "\texpr: " + expr + "\tstuckAtOne: " + stuckAtOne + "\tisSame: " + (expr == stuckAtOne));
		}
	}

	private static boolean expression(boolean x0, boolean x1, boolean x2, boolean x3) {
		//Aufgabe 1
		//return !x3 && x2 && !x1 || !x3 && x2 & x0 || x3 && x1 && !x0 || x3 && !x2;
		//		return !x2 && !x1 && x0 || !x3 && x1 || x1 && !x0 || !x2 && x1 /*|| !x3 && x2 && x0*/;
		//Aufgabe 5
		return !x3 && !x2 && x1 && x0 || x2 && x1 && !x0 || x3 && x2 && x0 || x3 && x2 && x1;
	}

	private static boolean stuckAtOne(boolean x0, boolean x1, boolean x2, boolean x3) {
		//Aufgabe 1
		//return !x3 && x2 && !x1 && !x0 || !x3 && x2 && !x1 && x0 || !x3 && x2 && x1 && x0 || x3 && x2 && x1 && !x0 || x3 && !x2 && !x1 && !x0 || x3
		//&& !x2 && !x1 && x0 || x3 && !x2 && x1 && !x0 || x3 && !x2 && x1 && x0;
		//Aufgabe 2
		//return !x3 && !x2 && x1 && x0 || !x3 && x2 && x1 && x0 || !x3 && x2 && x1 && !x0 || x3 && x2 && x1 && !x0 || x3 && !x2 && x1 && x0 || x3 &&
		//!x2 && x1 && !x0 || x3 && !x2 && !x1 && x0 || (!x3 && !x2 && !x1 && x0 || !x3 && !x2 && x1 && !x0 || !x3 && x2 && !x1 && !x0);
		//Aufgabe 5
		return !x3 && !x2 && x1 && x0 || !x3 && x2 && x1 && !x0 || x3 && x2 && !x1 && x0 || x3 && x2 && x1 && !x0 || x3 && x2 && x1 && x0;
	}

}
