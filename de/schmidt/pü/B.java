package de.schmidt.pü;

public class B extends A {

	int x = 17;

	public B() {                           // Signatur:B()

		super(1);

		new A(2);

		x++;

	}

	public B(int x) {                       // Signatur:B(I)

		super(x);

		y = x + 1;

		super.x = y + 1;

	}


	public void f(int i, B o) {}             // Signatur:B.f(IB)

	public void f(long lo, B o) {}            // Signatur:B.f(LB)

	public void f(long lo, A o) {}            // Signatur:B.f(LA)

}