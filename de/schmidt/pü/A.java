package de.schmidt.pü;

public class A {

	public static int y = 0;

	public int x = 23;

	public A(long x) {                      // Signatur:A(L)

	}


	public A(int x) {                       // Signatur:A(I)

		this(new Long(x));

		y += x;

	}


	public void f(int i, A o) {}             // Signatur:A.f(IA)

	public void f(long lo, A o) {}             // Signatur:A.f(LA)

	public void f(byte b, B o) {}             // Signatur:A.f(Byte B)

}
