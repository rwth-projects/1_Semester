package de.schmidt.pü;


public class C {

	public static void main(String[] args) {

		// a)

		A aa1 = new A(1L);                    // (1)

		System.out.println(aa1.x);

		System.out.println(A.y);

		A aa2 = new A(42);                    // (2)

		System.out.println(aa2.y);

		B bb = new B();                      // (3)

		System.out.println(bb.x);

		System.out.println(((A) bb).x);

		System.out.println(bb.y);

		A ab = new B(3);                     // (4)

		System.out.println(ab.x);

		System.out.println(((B) ab).x);

		System.out.println(A.y);


		// b)

		int i = 1;

		long lo = 2;

		byte b = 3;

		aa1.f(i, ab);                        // (1)

		aa1.f(lo, bb);                       // (2)

		aa1.f(b, ab);                        // (3)
		bb.f(i, bb);                        // (4)

		bb.f(lo, ab);                        // (5)

		bb.f(lo, bb);                        // (6)

		ab.f(b, bb);                        // (7)

		ab.f(lo, bb);                        // (8)

	}
}