package de.schmidt.vklausur;

/**
 * Created by Marc Schmidt on 25.02.2017.
 */
public class Child extends Parent {

	static int mY = 14;
	int mX = 7;

	Child(){
		System.out.println("Child");
	}

	Child(int pX){
		super(pX);
		System.out.println("Child");
	}

	void set(int x){
		System.out.println("Child, set: " + x + " int");
	}

	void set(long x){
		System.out.println("Child, set: " + x + " long");
	}

	void set(double x){
		System.out.println("Child, set: " + x + " double");
	}

	void hello(int x){
		System.out.println("Child: hello, Int");
	}

	void hello(float x){
		System.out.println("Child: hello, float");
	}

	void goodbye(float x){
		System.out.println("Child: goodbye");
	}
}
