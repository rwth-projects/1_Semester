package de.schmidt.task8_1;

/**
 * Created by Marc Schmidt on 16.12.2016.
 */
public class C {

	public static void main(String[] args) { // a) 4
		A aa1 = new A(1L);
		System.out.println(aa1.x);
		System.out.println(A.y);
		A aa2 = new A(42);
		System.out.println(aa2.y);
		B bb = new B();
		System.out.println(bb.x);
		System.out.println("c: "+((A) bb).x);
		System.out.println(bb.y);
		A ab = new B(3);
		System.out.println("d: "+ab.x);
		System.out.println(((B) ab).x);
		System.out.println(A.y);
		// b)
		int i = 1;
		long lo = 2;
		byte b = 3;
		aa1.f(i, ab);
		aa1.f(lo, bb);
		aa1.f(b, ab);
		bb.f(i, bb);
		bb.f(lo, ab);
		bb.f(lo, bb);
		ab.f(b, bb);
		ab.f(lo, bb);
	}
}
