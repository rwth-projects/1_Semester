package de.schmidt.ds.kombinatorik;

/**
 * Created by Marc Schmidt on 07.02.2017.
 */
public class Wahrscheinlichkeit {

	private static final Menge<Text> WUERFEL = Combine.create('1', '2', '3', '4', '5', '6');
	private static final Text EINS = new Text("1");
	private static final Text ZWEI = new Text("2");
	private static final Text DREI = new Text("3");
	private static final Text VIER = new Text("4");
	private static final Text FUENF = new Text("5");
	private static final Text SECHS = new Text("6");

	public static void main(String[] args) {
		//		new Thread(Wahrscheinlichkeit:: nr73_1).start();
		//		new Thread(Wahrscheinlichkeit:: nr73_2).start();
		//		new Thread(Wahrscheinlichkeit:: nr73_3).start();
		//		new Thread(Wahrscheinlichkeit:: nr73_4).start();
		//		new Thread(Wahrscheinlichkeit:: nr73_5).start();
		//		new Thread(Wahrscheinlichkeit:: nr73_6).start();
		new Thread(Wahrscheinlichkeit:: nr73_marco_1).start();
//		new Thread(Wahrscheinlichkeit:: nr73_marco_2).start();
//		new Thread(Wahrscheinlichkeit:: nr73_marco_3).start();
//		new Thread(Wahrscheinlichkeit:: nr73_marco_4).start();
//		new Thread(Wahrscheinlichkeit:: nr73_marco_5).start();
//		new Thread(Wahrscheinlichkeit:: nr73_marco_6).start();
	}

	private static void nr73_marco_1() {
		int wuerfel = 6;
		Filter<Text> filter = new Filter<Text>() {

			@Override
			public boolean filter(Menge<Text> pMenge) {
				return pMenge.count(VIER) <= 3;
			}
		};
		double resFilter = Operationen.variationCount(wuerfel, WUERFEL, filter);
		double resAll = Operationen.variationCount(wuerfel, WUERFEL);
		double res = resFilter / resAll;
		System.out.println("-- 73) 1) --");
		System.out.println("res = " + res + ", round: " + roundScale4(res));
		System.out.println("");
	}

	private static void nr73_marco_2() {
		int wuerfel = 9;
		Filter<Text> filter = new Filter<Text>() {

			@Override
			public boolean filter(Menge<Text> pMenge) {
				return pMenge.count(ZWEI) >= 5;
			}
		};
		double resFilter = Operationen.variationCount(wuerfel, WUERFEL, filter);
		double resAll = Operationen.variationCount(wuerfel, WUERFEL);
		double res = resFilter / resAll;
		System.out.println("-- 73) 2) --");
		System.out.println("res = " + res + ", round: " + roundScale4(res));
		System.out.println("");
	}

	private static void nr73_marco_3() {
		int wuerfel = 7;
		Filter<Text> filter = new Filter<Text>() {

			@Override
			public boolean filter(Menge<Text> pMenge) {
				return pMenge.count(VIER) == 0;
			}
		};
		double resFilter = Operationen.variationCount(wuerfel, WUERFEL, filter);
		double resAll = Operationen.variationCount(wuerfel, WUERFEL);
		double res = resFilter / resAll;
		System.out.println("-- 73) 3) --");
		System.out.println("res = " + res + ", round: " + roundScale4(res));
		System.out.println("");
	}

	private static void nr73_marco_4() {
		int wuerfel = 12;
		Filter<Text> filter = new Filter<Text>() {

			@Override
			public boolean filter(Menge<Text> pMenge) {
				return pMenge.count(EINS) >= 1 && pMenge.count(SECHS) >= 1;
			}
		};
		double resFilter = Operationen.variationCount(wuerfel, WUERFEL, filter);
		double resAll = Operationen.variationCount(wuerfel, WUERFEL);
		double res = resFilter / resAll;
		System.out.println("-- 73) 4) --");
		System.out.println("res = " + res + ", round: " + roundScale4(res));
		System.out.println("");
	}

	private static void nr73_marco_5() {
		int wuerfel = 6;
		Filter<Text> filter = new Filter<Text>() {

			@Override
			public boolean filter(Menge<Text> pMenge) {
				return pMenge.count(EINS) >= 1 || pMenge.count(VIER) >= 1;
			}
		};
		double resFilter = Operationen.variationCount(wuerfel, WUERFEL, filter);
		double resAll = Operationen.variationCount(wuerfel, WUERFEL);
		double res = resFilter / resAll;
		System.out.println("-- 73) 5) --");
		System.out.println("res = " + res + ", round: " + roundScale4(res));
		System.out.println("");
	}

	private static void nr73_marco_6() {
		int wuerfel = 6;
		Filter<Text> filter = new Filter<Text>() {

			@Override
			public boolean filter(Menge<Text> pMenge) {
				return pMenge.count(EINS) == 2;
			}
		};
		double resFilter = Operationen.variationCount(wuerfel, WUERFEL, filter);
		double resAll = Operationen.variationCount(wuerfel, WUERFEL);
		double res = resFilter / resAll;
		System.out.println("-- 73) 6) --");
		System.out.println("res = " + res + ", round: " + roundScale4(res));
		System.out.println("");
	}

	private static void nr73_1() {
		int wuerfel = 12;
		Filter<Text> filter = new Filter<Text>() {

			@Override
			public boolean filter(Menge<Text> pMenge) {
				return pMenge.count(EINS) == 0;
			}
		};
		double resFilter = Operationen.variationCount(wuerfel, WUERFEL, filter);
		double resAll = Operationen.variationCount(wuerfel, WUERFEL);
		double res = resFilter / resAll;
		System.out.println("-- 73) 1) --");
		System.out.println("res = " + res + ", round: " + roundScale4(res));
		System.out.println("");
	}

	private static void nr73_2() {
		int wuerfel = 5;
		Filter<Text> filter = new Filter<Text>() {

			@Override
			public boolean filter(Menge<Text> pMenge) {
				return pMenge.count(EINS) <= 2;
			}
		};
		double resFilter = Operationen.variationCount(wuerfel, WUERFEL, filter);
		double resAll = Operationen.variationCount(wuerfel, WUERFEL);
		double res = resFilter / resAll;
		System.out.println("-- 73) 2) --");
		System.out.println("res = " + res + ", round: " + roundScale4(res));
		System.out.println("");
	}

	private static void nr73_3() {
		int wuerfel = 9;
		Filter<Text> filter = new Filter<Text>() {

			@Override
			public boolean filter(Menge<Text> pMenge) {
				return pMenge.count(VIER) >= 6;
			}
		};
		double resFilter = Operationen.variationCount(wuerfel, WUERFEL, filter);
		double resAll = Operationen.variationCount(wuerfel, WUERFEL);
		double res = resFilter / resAll;
		System.out.println("-- 73) 3) --");
		System.out.println("res = " + res + ", round: " + roundScale4(res));
		System.out.println("");
	}

	private static void nr73_4() {
		int wuerfel = 9;
		Filter<Text> filter = new Filter<Text>() {

			@Override
			public boolean filter(Menge<Text> pMenge) {
				return pMenge.count(ZWEI) == 6;
			}
		};
		double resFilter = Operationen.variationCount(wuerfel, WUERFEL, filter);
		double resAll = Operationen.variationCount(wuerfel, WUERFEL);
		double res = resFilter / resAll;
		System.out.println("-- 73) 4) --");
		System.out.println("res = " + res + ", round: " + roundScale4(res));
		System.out.println("");
	}

	private static void nr73_5() {
		int wuerfel = 6;
		Filter<Text> filter = new Filter<Text>() {

			@Override
			public boolean filter(Menge<Text> pMenge) {
				return pMenge.count(VIER) >= 1 || pMenge.count(ZWEI) >= 1;
			}
		};
		double resFilter = Operationen.variationCount(wuerfel, WUERFEL, filter);
		double resAll = Operationen.variationCount(wuerfel, WUERFEL);
		double res = resFilter / resAll;
		System.out.println("-- 73) 5) --");
		System.out.println("res = " + res + ", round: " + roundScale4(res));
	}

	private static void nr73_6() {
		int wuerfel = 7;
		Filter<Text> filter = new Filter<Text>() {

			@Override
			public boolean filter(Menge<Text> pMenge) {
				return pMenge.count(FUENF) >= 1 || pMenge.count(DREI) >= 1;
			}
		};
		double resFilter = Operationen.variationCount(wuerfel, WUERFEL, filter);
		double resAll = Operationen.variationCount(wuerfel, WUERFEL);
		double res = resFilter / resAll;
		System.out.println("-- 73) 6) --");
		System.out.println("res = " + res + ", round: " + roundScale4(res));
		System.out.println("");
	}

	private static double roundScale4(double d) {
		return Math.round(d * 10000) / 10000D;
	}
}
