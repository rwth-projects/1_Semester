package de.schmidt.ds.kombinatorik;

import java.util.ArrayList;

/**
 * Created by Marc Schmidt on 20.01.2017.
 */
public class Operationen {

	private static long sTime = System.currentTimeMillis();

	public static <G extends IElement> Menge<G> vereinigen(Menge<G>... pMengen) {
		int size = 0;
		for (Menge<G> menge : pMengen) {
			size += menge.size();
		}
		Menge<G> vereinigung = new Menge<>(size);
		for (Menge<G> menge : pMengen) {
			addSave(vereinigung, menge);
		}
		return vereinigung;
	}

	private static <G extends IElement> void addSave(Menge<G> pListStatic, Menge<G> pList2) {
		for (G element : pList2.getElements()) {
			addSave(pListStatic, element);
		}
	}

	private static <G extends IElement> void addSave(Menge<G> pElements, G pElement) {
		if (!pElements.contains(pElement)) {
			pElements.add(pElement);
		}
	}

	public static <G extends IElement> Menge<Menge<G>> variation(int pK, Menge<G> pPool) {
		return variation(pK, pPool, Filter.addAll());
	}

	public static <G extends IElement> long variationCount(int pK, Menge<G> pPool) {
		return variationCount(pK, pPool, Filter.addAll());
	}

	public static <G extends IElement> long variationCount(int pK, Menge<G> pPool, Filter<G> pFilter) {
		long count = 0;
		int calculated = 0;
		long einProzent = (long) (Math.pow(pPool.size(), pK) / 100);
//		System.out.println("einProzent = " + einProzent);
		double prozent = 100 / Math.pow(pPool.size(), pK);
		Menge<G> start = new Menge<>(pK);
		Menge<G> cancel = new Menge<>(pK);
		for (int i = 0; i < pK; i++) {
			start.add(pPool.get(0));
			cancel.add(pPool.get(pPool.size() - 1));
		}

		while (!start.variationGleich(cancel)) {
			if (pFilter.filter(start)) {
				count++;
			}
			start = start.iterate(start, pPool, true);
			calculated++;
//			if (calculated % einProzent == 0) {
//				System.out.println("calculated: " + (prozent * calculated) + ", time: " + ((System.currentTimeMillis() - sTime) / 1000));
//			}
		}
		if (pFilter.filter(cancel)) {
			count++;
		}

		return count;
	}

	public static <G extends IElement> Menge<Menge<G>> variation(int pK, Menge<G> pPool, Filter<G> pFilter) {
		double prozent = 100 / Math.pow(pPool.size(), pK);
		Menge<Menge<G>> menge = new Menge<>(2998800);
		Menge<G> start = new Menge<>(pK);
		Menge<G> cancel = new Menge<>(pK);
		for (int i = 0; i < pK; i++) {
			start.add(pPool.get(0));
			cancel.add(pPool.get(pPool.size() - 1));
		}

		while (!start.variationGleich(cancel)) {
			int calculated = 0;
			if (pFilter.filter(start)) {
				menge.add(start);
			}
			start = start.iterate(start, pPool, true);
			calculated++;
			if (calculated % 1000000 == 0) {
				System.out.println("calculated: " + (prozent * calculated));
			}
		}
		if (pFilter.filter(cancel)) {
			menge.add(cancel);
		}

		return menge;
	}

	public static <G extends IElement> Menge<Menge<G>> filter(Filter<G> pFilter, Menge<Menge<G>> pMenge, boolean pInclude) {
		Menge<Menge<G>> filtered = new Menge<>(pMenge.size() / 10);
		ArrayList<Menge<G>> elements = pMenge.getElements();
		for (Menge<G> element : elements) {
			if (pInclude == pFilter.filter(element)) {
				filtered.add(element);
			}
		}
		return filtered;
	}

	public static <G extends IElement> int filterCount(Filter<G> pFilter, Menge<Menge<G>> pMenge, boolean pInclude) {
		int count = 0;
		ArrayList<Menge<G>> elements = pMenge.getElements();
		for (Menge<G> element : elements) {
			if (pInclude == pFilter.filter(element)) {
				count++;
			}
		}
		return count;
	}

	public static <G extends IElement> Menge<Menge<G>> multiComb(int pK, Menge<G> pPool, G pCancel) {
		return multiComb(pK, pPool, Filter.addAll(), pCancel);
	}

	public static <G extends IElement> Menge<Menge<G>> multiComb(int pK, Menge<G> pPool, Filter<G> pFilter, G pCancel) {
		double prozent = 100 / (factorial(pK + pPool.size() - 1) / factorial(pK) * factorial(pPool.size() - pK));
		Menge<Menge<G>> menge = new Menge<>(200);
		Menge<G> start = new Menge<>(pK);
		Menge<G> cancel = new Menge<>(pK);
		for (int i = 0; i < pK; i++) {
			start.add(pPool.get(0));
			cancel.add(pPool.get(pPool.size() - 1));
		}

		int calculated = 0;
		while (!combCancel(start, pCancel)) {
			if (pFilter.filter(start)) {
				menge.add(start);
			}
			start = start.iterate(start, pPool, false);
			calculated++;
			if (calculated % 10000000 == 0) {
				System.out.println("calculated: " + (prozent * calculated));
			}
		} if (pFilter.filter(cancel)) {
			menge.add(cancel);
		}

		return menge;
	}

	//	private static final Text sechs = new Text("6");

	private static <G extends IElement> boolean combCancel(Menge<G> pElement, G pCancel) {
		for (int i = 0; i < pElement.size(); i++) {
			if (!pElement.get(i).gleich(pCancel)) {
				return false;
			}
		}
		return true;
	}

	public static long factorial(long n) {
		if (n <= 1) { return 1; } else { return n * factorial(n - 1); }
	}
}
