package de.schmidt.ds.kombinatorik;

import java.util.ArrayList;

/**
 * Created by Marc Schmidt on 20.01.2017.
 */
public class Menge<T extends IElement> implements IElement {

	private ArrayList<T> mElements;

	public Menge(ArrayList<T> pElements) {
		mElements = new ArrayList<>(pElements);
	}

	public Menge(Menge<T> pMenge) {
		this(pMenge.mElements);
	}

	public Menge(int pCapacity) {
		mElements = new ArrayList<>(pCapacity);
	}

	public Menge(T[] pArr) {
		mElements = new ArrayList<>(pArr.length);
		for (T element : pArr) {
			mElements.add(element);
		}
	}

	public int count(T pElement) {
		int count = 0;
		for (T element : mElements) {
			if (element.gleich(pElement)) {
				count++;
			}
		}
		return count;
	}

	public void add(T pElement) {
		mElements.add(pElement);
	}

	public static <G extends IElement> Menge<G> iterate(Menge<G> pMenge, Menge<G> pPool, boolean pReset) {
		Menge<G> menge = new Menge<>(pMenge);
		int index = menge.lastIndexIterate(pPool, pReset);
		if (index == -1) {
			return null;
		}

		if (pReset) {
			menge.set(index, pMenge.getNext(pPool, pMenge.mElements.get(index)));
		} else {
			for (int i = index; i < pMenge.size(); i++) {
				G next = pMenge.getNext(pPool, pMenge.mElements.get(index));
				menge.set(i, next);
			}
		}

		return menge;
	}

	private T getNext(Menge<T> pPool, T pElement) {
		for (int i = 0; i < pPool.size() - 1; i++) {
			if (pPool.get(i).gleich(pElement)) {
				return pPool.get(i + 1);
			}
		}
		return null;
	}

	private int lastIndexIterate(Menge<T> pPool, boolean pReset) {
		for (int i = size() - 1; i >= 0; i--) {
			if (hasNext(mElements.get(i), pPool)) {
				return i;
			}
			mElements.set(i, pPool.get(0));
		}
		return -1;
	}

	private boolean hasNext(T pElement, Menge<T> pPool) {
		int index = getIndex(pElement, pPool.mElements);
		return index != -1 && index < pPool.size() - 1;
	}

	private int getIndex(T pElement, ArrayList<T> pPool) {
		int index = 0;
		for (T element : pPool) {
			if (element.gleich(pElement)) {
				return index;
			}
			index++;
		}
		return -1;
	}

	public boolean contains(T pElement) {
		for (T element : mElements) {
			if (element.gleich(pElement)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Entsprechend einer Variation
	 */
	public boolean variationGleich(Menge<T> pMenge) {
		if (size() == pMenge.size()) {
			for (int i = 0; i < size(); i++) {
				if (!mElements.get(i).gleich(pMenge.get(i))) {
					return false;
				}
			}
			return true;
		}
		return false;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder().append('{');
		for (int i = 0; i < mElements.size(); i++) {
			sb.append(mElements.get(i).toText());
			if (i < mElements.size() - 1) {
				sb.append(',');
			}
		}
		return sb.append('}').toString();
	}

	public ArrayList<T> getElements() {
		return mElements;
	}

	public void set(int pIndex, T pElement) {
		mElements.set(pIndex, pElement);
	}

	public T get(int pIndex) {
		return mElements.get(pIndex);
	}

	public int size() {
		return mElements.size();
	}

	@Override
	public <T> boolean gleich(T pElement) {
		return false;
	}

	@Override
	public String toText() {
		return toString();
	}
}
