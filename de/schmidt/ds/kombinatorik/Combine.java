package de.schmidt.ds.kombinatorik;

/**
 * Created by Marc Schmidt on 20.01.2017.
 */
public class Combine {

	private static final char[] GROSSBUCHSTABEN = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S',
			'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
	private static final char[] KLEINBUCHSTABEN = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
			't', 'u', 'v', 'w', 'x', 'y', 'z'};
	private static final char[] ZIFFERN = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
	private static final char[] SONDERZEICHEN = {'!', '"', '#', '$', '%', '&', '\'', '(', ')', '*', '+', ',', '-', '.', ':', ';', '<', '=', '>',
			'?', '[', '\\', ']', '^', '_', '@', '{', '|', '}', '~', '`', '\t', '\n'};

	public static void main(String[] args) {
		Menge<Text> grossbuchstaben = create(GROSSBUCHSTABEN);
		Menge<Text> kleinbuchstanen = create(KLEINBUCHSTABEN);
		Menge<Text> ziffern = create(ZIFFERN);
		Menge<Text> sonderzeichen = create(SONDERZEICHEN);

		Menge<Text> abcd = create('a', 'b', 'c', 'd');
		//		Menge<Menge<Text>> var = Operationen.variation(3, abcd);
		//		System.out.println(var.size());
		//		System.out.println(Operationen.variationCount(3, abcd));

		/**
		 * Wie viele Passwörter bestehend aus fünf Zeichen enthalten lediglich Groß und Kleinbuchstaben, (mindestens) einmal den Großbuchstaben V
		 * und kein Zeichen mehrfach?
		 */
		new Thread(() -> {
			Menge<Text> grossKleinBuchstaben = Operationen.vereinigen(grossbuchstaben, kleinbuchstanen);
			Filter<Text> filter_64_1 = new Filter<Text>() {

				private final Text TEXT_V = new Text("V");

				@Override
				public boolean filter(Menge<Text> pMenge) {
					for (int i = 0; i < grossKleinBuchstaben.size(); i++) {
						if (pMenge.count(grossKleinBuchstaben.get(i)) > 1) {
							return false;
						}
					}
					return pMenge.contains(TEXT_V);
				}
			};
			System.out.println("64_1 res: " + Operationen.variationCount(3, grossKleinBuchstaben, filter_64_1));
		})/*.start()*/;

		/**
		 * Wieviele Passwörter bestehend aus höchstens fünf Zeichen enthalten das Sonderzeichen ??
		 */
		new Thread(() -> {
			Menge<Text> zeichen = Operationen.vereinigen(grossbuchstaben, kleinbuchstanen, ziffern, sonderzeichen);
			Filter<Text> filter_64_2 = new Filter<Text>() {

				private final Text TEXT_FRAGEZEICHEN = new Text("?");

				@Override
				public boolean filter(Menge<Text> pMenge) {
					return pMenge.contains(TEXT_FRAGEZEICHEN);
				}
			};
			int res_64_2 = 0;
			for (int i = 0; i <= 5; i++) {
				System.out.println("i: " + i);
				res_64_2 += Operationen.variationCount(i, zeichen, filter_64_2);
			}
			System.out.println("64_2 res: " + res_64_2);
		})/*.start()*/;

		/**
		 * Wie viele Passwörter bestehend aus fünf Kleinbuchstaben gefolgt von sechs Großbuchstaben enthalten keine Großbuchstaben mehrfach?
		 */
		new Thread(() -> {
			long kleinKomb = Operationen.variationCount(5, kleinbuchstanen);

			Filter<Text> filter_64_3 = new Filter<Text>() {
				@Override
				public boolean filter(Menge<Text> pMenge) {
					for (int i = 0; i < pMenge.size(); i++) {
						if (pMenge.count(pMenge.get(i)) > 1) {
							return false;
						}
					}
					return true;
				}
			};
			long grossKomb = Operationen.variationCount(6, grossbuchstaben, filter_64_3);
			System.out.println("kleinKomb = " + kleinKomb);
			System.out.println("grossKomb = " + grossKomb);
			System.out.println("res: " + (kleinKomb * grossKomb));
		})/*.start()*/;

		/**
		 * Wie viele Passwörter bestehend aus höchstens acht Zeichen enthalten lediglich Groß- und Kleinbuchstaben und Sonderzeichen?
		 */
		new Thread(() -> {
			long res = 0;
			Menge<Text> zeichen = Operationen.vereinigen(kleinbuchstanen, grossbuchstaben, sonderzeichen);
			for (int i = 0; i <= 8; i++) {
				long cur = Operationen.variationCount(i, zeichen);
				System.out.println("i: " + i + ", " + cur);
				res += cur;
			}
			System.out.println("res: " + res);
		})/*.start()*/;

		/**
		 * Wie viele Passwörter bestehend aus acht Zeichen enthalten (mindestens) einen Großbuchstaben und (mindestens) einen Kleinbuchstaben?
		 */
		new Thread(() -> {
			Menge<Text> zeichen = Operationen.vereinigen(grossbuchstaben, kleinbuchstanen, ziffern, sonderzeichen);
			Filter<Text> filter_64_5 = new Filter<Text>() {

				@Override
				public boolean filter(Menge<Text> pMenge) {
					boolean containsGross = false;
					for (int j = 0; j < grossbuchstaben.size(); j++) {
						if (pMenge.contains(grossbuchstaben.get(j))) {
							containsGross = true;
							break;
						}
					}
					boolean containsKlein = false;
					for (int j = 0; j < kleinbuchstanen.size(); j++) {
						if (pMenge.contains(kleinbuchstanen.get(j))) {
							containsKlein = true;
							break;
						}
					}
					return containsGross && containsKlein;
				}
			};
			long cur = Operationen.variationCount(3, zeichen, filter_64_5);
			System.out.println("res = " + cur);
		})/*.start()*/;

		/**
		 * Wie viele Passwörter bestehend aus sieben Zeichen enthalten (mindestens) ein Zeichen mehrfach?
		 */
		new Thread(() -> {
			Menge<Text> zeichen = Operationen.vereinigen(grossbuchstaben, kleinbuchstanen, ziffern, sonderzeichen);
			Filter<Text> filter_64_6 = new Filter<Text>() {
				@Override
				public boolean filter(Menge<Text> pMenge) {
					for (int i = 0; i < pMenge.size(); i++) {
						if (pMenge.count(pMenge.get(i)) > 1) {
							return true;
						}
					}
					return false;
				}
			};
			long res = Operationen.variationCount(3, zeichen, filter_64_6);
			System.out.println("3 Zeichen mind 2: " + res);
		})/*.start()*/;

		Menge<Text> wuerfel = create('1', '2', '3', '4', '5', '6');
		Text sechs = new Text("6");

		/**
		 * Wie viele Würfe mit neun Würfeln enthalten genau zweimal eine 4?
		 */
		new Thread(() -> {
			Filter<Text> filter_65_1 = new Filter<Text>() {

				@Override
				public boolean filter(Menge<Text> pMenge) {
					return pMenge.count(new Text("4")) == 2;
				}
			};
			Menge<Menge<Text>> comb = Operationen.multiComb(9, wuerfel, filter_65_1, sechs);
			for (int i = 0; i < comb.size(); i++) {
				System.out.println(comb.get(i));
			}
			System.out.println("size: " + comb.size());
		})/*.start()*/;

		/**
		 * Wie viele Würfe mit sechs Würfeln enthalten (mindestens) eine 3 oder (mindestens) eine 5 (oder beide)?
		 */
		new Thread(() -> {
			Filter<Text> filter_65_2 = new Filter<Text>() {

				@Override
				public boolean filter(Menge<Text> pMenge) {
					return pMenge.count(new Text("3")) >= 1 || pMenge.count(new Text("5")) >= 1;
				}
			};
			Menge<Menge<Text>> comb = Operationen.multiComb(6, wuerfel, filter_65_2, sechs);
			for (int i = 0; i < comb.size(); i++) {
				System.out.println(comb.get(i));
			}
			System.out.println("size: " + comb.size());
		})/*.start()*/;

		/**
		 * Wie viele Würfe mit fünf Würfeln enthalten genau eine 1 oder (mindestens) eine 6 (oder beide)?
		 */
		new Thread(() -> {
			Filter<Text> filter_65_2 = new Filter<Text>() {

				@Override
				public boolean filter(Menge<Text> pMenge) {
					return pMenge.count(new Text("1")) == 1 || pMenge.count(new Text("6")) >= 1;
				}
			};
			Menge<Menge<Text>> comb = Operationen.multiComb(5, wuerfel, filter_65_2, sechs);
			for (int i = 0; i < comb.size(); i++) {
				System.out.println(comb.get(i));
			}
			System.out.println("size: " + comb.size());
		})/*.start()*/;

		/**
		 * Wie viele Würfe mit neun Würfeln enthalten (mindestens) eine 3 und (mindestens) eine 5?
		 */
		new Thread(() -> {
			Filter<Text> filter_65_2 = new Filter<Text>() {

				@Override
				public boolean filter(Menge<Text> pMenge) {
					return pMenge.count(new Text("3")) >= 1 && pMenge.count(new Text("5")) >= 1;
				}
			};
			Menge<Menge<Text>> comb = Operationen.multiComb(9, wuerfel, filter_65_2, sechs);
			for (int i = 0; i < comb.size(); i++) {
				System.out.println(comb.get(i));
			}
			System.out.println("size: " + comb.size());
		})/*.start()*/;

		/**
		 * Wie viele PINs bestehend aus (genau) vier arabischen Ziffern beginnen nicht mit einer Null und enthalten keine Ziffer mehrfach?
		 */
		new Thread(() -> {
			Filter<Text> filter_67_b = new Filter<Text>() {

				@Override
				public boolean filter(Menge<Text> pMenge) {
					for (int i = 0; i < pMenge.size(); i++) {
						if (pMenge.count(pMenge.get(i)) > 1) {
							return false;
						}
					}
					return !pMenge.get(0).gleich(new Text("0"));
				}
			};
			Menge<Menge<Text>> pin = Operationen.variation(4, ziffern, filter_67_b);
			for (int i = 0; i < pin.size(); i++) {
				System.out.println(pin.get(i).toText());
			}
			System.out.println(pin.size());
		})/*.start()*/;

		/**
		 * Eine Softwareﬁrma hat 23 Apps im Verkauf, darunter sieben Bestseller. Wie viele Möglichkeiten gibt es, ein Bundle aus zehn Apps
		 * zusammenzustellen, wenn dieses Bundle mindestens zwei und höchstens drei Bestseller enthalten soll?
		 */
		new Thread(() -> {
			Menge<Text> bestseller = create("1234567");
			Menge<Text> bundle = Operationen.vereinigen(bestseller, create("abcdefghijklmnopqr"));
			System.out.println("bundle.size(): " + bundle.size());
			Filter<Text> filter_67_a = new Filter<Text>() {
				@Override
				public boolean filter(Menge<Text> pMenge) {
					for (int i = 0; i < pMenge.size(); i++) {
						if (pMenge.count(pMenge.get(i)) > 1) {
							return false;
						}
					}
					int bestQuan = 0;
					for (int i = 0; i < bestseller.size(); i++) {
						bestQuan += pMenge.count(bestseller.get(i));
					}
					return bestQuan == 2 || bestQuan == 3;
					//					return true;
				}
			};

			Menge<Menge<Text>> bundles = Operationen.multiComb(10, bundle, filter_67_a, new Text("r"));
			for (int i = 0; i < bundles.size(); i += 1000) {
				System.out.println(bundles.get(i).toText());
			}
			System.out.println(bundles.get(bundles.size() - 1).toText() + "_");
			System.out.println(bundles.size());
		})/*.start()*/;

		//TODO
		/**
		 * Wie viele Würfe mit neun Würfeln enthalten (mindestens) eine 3 und (mindestens) eine 5?
		 */
		new Thread(() -> {
			Filter<Text> filter_65_2 = new Filter<Text>() {

				@Override
				public boolean filter(Menge<Text> pMenge) {
					return pMenge.count(new Text("3")) >= 1 && pMenge.count(new Text("5")) >= 1;
					//					return true;
				}
			};
			Menge<Menge<Text>> comb = Operationen.multiComb(9, wuerfel, filter_65_2, sechs);
			for (int i = 0; i < comb.size(); i++) {
				System.out.println(comb.get(i));
			}
			System.out.println("size: " + comb.size());
		})/*.start()*/;


		new Thread(() -> {
			Text a = new Text("a");
			Text p = new Text("p");
			Text f = new Text("f");
			Text e = new Text("e");
			Text l = new Text("l");
			Text k = new Text("k");
			Text o = new Text("o");
			Text m = new Text("m");
			Text t = new Text("t");
			Filter<Text> filter = new Filter<Text>() {

				@Override
				public boolean filter(Menge<Text> pMenge) {
					return pMenge.count(a) == 1 && pMenge.count(p) == 2 && pMenge.count(f) == 1 && pMenge.count(e) == 1 && pMenge.count(l) == 1 &&
							pMenge.count(k) == 1 && pMenge.count(o) == 2 && pMenge.count(m) == 1 && pMenge.count(t) == 2;
				}
			};
			long var = /*Menge<Menge<Text>> comb =*/ Operationen.variationCount(12, create("apfelkomt"), filter);
	/*		for (int i = 0; i < comb.size(); i++) {
				System.out.println(comb.get(i));
			}
			System.out.println("size: " + comb.size());*/
			System.out.println("var: " + var);
		}).start();
	}

	private static Menge<Text> create(String pStr) {
		Menge<Text> menge = new Menge<>(pStr.length());
		for (int i = 0; i < pStr.length(); i++) {
			menge.add(new Text(Character.toString(pStr.charAt(i))));
		}
		return menge;
	}

	public static Menge<Text> create(char... pArr) {
		Menge<Text> menge = new Menge<>(pArr.length);
		for (char ch : pArr) {
			menge.add(new Text(Character.toString(ch)));
		}
		return menge;
	}

}
