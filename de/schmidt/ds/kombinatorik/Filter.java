package de.schmidt.ds.kombinatorik;

/**
 * Created by Marc Schmidt on 21.01.2017.
 */
public abstract class Filter<T extends IElement> {

	public abstract boolean filter(Menge<T> pMenge);

	public static <G extends IElement> Filter<G> addAll() {
		return new Filter<G>() {

			@Override
			public boolean filter(Menge<G> pMenge) {
				return true;
			}
		};
	}
}
