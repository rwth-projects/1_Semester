package de.schmidt.ds.kombinatorik;

/**
 * Created by Marc Schmidt on 20.01.2017.
 */
public interface IElement {

	<T> boolean gleich(T pElement);

	String toText();

}
