package de.schmidt.ds.kombinatorik;

/**
 * Created by Marc Schmidt on 20.01.2017.
 */
public class Text implements IElement {

	private String mText;

	public Text(String pText) {
		mText = pText;
	}

	@Override
	public <T> boolean gleich(T pElement) {
		if (pElement instanceof Text) {
			String text = ((Text) pElement).mText;
			if (mText.length() == text.length()) {
				for (int i = 0; i < mText.length(); i++) {
					if (mText.charAt(i) != text.charAt(i)) {
						return false;
					}
				}
				return true;
			}
		}
		return false;
	}

	@Override
	public String toText() {
		return mText;
	}
}
