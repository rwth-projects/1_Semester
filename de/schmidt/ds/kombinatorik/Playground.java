package de.schmidt.ds.kombinatorik;

import java.math.BigInteger;

/**
 * Created by Marc Schmidt on 21.01.2017.
 */
public class Playground {

	public static void main(String[] args) {
		System.out.println(bioko(13,8));
	}

	private static BigInteger combAnz(BigInteger pPool, BigInteger k) {
		return factorial(pPool.add(k).subtract(new BigInteger("1"))).divide(factorial(k).multiply(factorial(pPool.subtract(k))));
	}

	private static long bioko(long pPool, long pK){
		return (factorial(pPool) / (factorial(pK) * factorial(pPool - pK)));
	}

	public static long factorial(long n) {
		if (n <= 1) { return 1; } else { return n * factorial(n - 1); }
	}

	public static BigInteger factorial(BigInteger n) {
		if (n.compareTo(new BigInteger("1")) <= 0) {
			return new BigInteger("1");
		} else {
			return n.multiply(factorial(n.subtract(new BigInteger("1"))));
		}
	}

	private static long calc(long x) {
		return (long) Math.pow(95, x) - 2 * (long) Math.pow(69, x) + (long) Math.pow(43, x);
	}

	private static double calc(long pBase, long pK) {
		long res = 0;
		for (int i = 0; i <= pK; i++) {
			res += Math.pow(pBase, i) - Math.pow(pBase - 1, i);
		}
		return res;
	}

	private static long calc1(long pBase, long pK) {
		long res = 0;
		for (int i = 0; i <= pK; i++) {
			res += Math.pow(pBase, i);
		}
		return res;
	}

}
