package de.schmidt.ds;

/**
 * Created by Marc Schmidt on 20.01.2017.
 */
public class Fraction {

	private static final int BOUNCE_HIGH = (Matrix.RESTKLASSENRING - 1) / 2;
	private static final int BOUNCE_LOW = -BOUNCE_HIGH;

	private int mNumerator;
	private int mDenominator;

	public Fraction(int pNumerator, int pDenominator) {
		mNumerator = pNumerator;
		mDenominator = pDenominator;
	}

	public Fraction calc(boolean pAddBounce) {
		int inv = -1;
		if (mDenominator == 1) {
			inv = 1;
		} else if (mDenominator == 2) {
			inv = 3;
		} else if (mDenominator == 3) {
			inv = 2;
		} else if (mDenominator == 9) {
			inv = 4;
		}
		return new Fraction(pAddBounce ? addToBounce(mNumerator * inv) : (mNumerator * inv), 1);
	}

	private int addToBounce(int pNum) {
		if (pNum > BOUNCE_HIGH) {
			do {
				pNum -= Matrix.RESTKLASSENRING;
			} while (pNum > BOUNCE_HIGH);
		} else if (pNum < BOUNCE_LOW) {
			do {
				pNum += Matrix.RESTKLASSENRING;
			} while (pNum < BOUNCE_LOW);
		}
		return pNum;
	}



	@Override
	public String toString() {
		if (mDenominator == 1) {
			return Integer.toString(mNumerator);
		} else {
			return mNumerator + "/" + mDenominator;
		}
	}
}
