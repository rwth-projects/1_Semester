package de.schmidt.ds;

/**
 * Created by Marc Schmidt on 20.01.2017.
 */
public class Matrix {

	public static final int RESTKLASSENRING = 5;

	private Fraction[][] mFractions;

	public static void main(String[] args) {
				Matrix matrix = new Matrix(4, 4);
				matrix.addRow(0, new int[] {-2, 2, 5, 2}, new int[] {3, 3, 9, 9});
				matrix.addRow(1, new int[] {5, -2, -8, -5}, new int[] {3, 3, 9, 9});
				matrix.addRow(2, new int[] {-1, 1, 4, -2}, new int[] {3, 3, 9, 9});
				matrix.addRow(3, new int[] {-1, 0, 2, 2}, new int[] {1, 1, 3, 3});
//		Matrix matrix = new Matrix(5, 5);
//		matrix.addRow(0, new int[] {});
//		matrix.addRow(0, new int[] {});
//		matrix.addRow(0, new int[] {});
//		matrix.addRow(0, new int[] {});
//		matrix.addRow(0, new int[] {});
		System.out.println(matrix);
		System.out.println(matrix.calc(false));
		System.out.println(matrix.calc(true));
		System.out.println("Ergebnis: "+matrix.calc(true).toText());
	}

	public Matrix(int pRow, int pColumns) {
		mFractions = new Fraction[pRow][pColumns];
	}

	public void addRow(int pRow, int[] pNumerator) {
		for (int i = 0; i < mFractions[pRow].length; i++) {
			mFractions[pRow][i] = new Fraction(pNumerator[i], 0);
		}
	}

	public void addRow(int pRow, int[] pNumerator, int[] pDenominator) {
		for (int i = 0; i < mFractions[pRow].length; i++) {
			mFractions[pRow][i] = new Fraction(pNumerator[i], pDenominator[i]);
		}
	}

	public void addCell(int pRow, int pColumn, Fraction pFraction) {
		mFractions[pRow][pColumn] = pFraction;
	}

	public String toText() {
		StringBuilder sb = new StringBuilder().append('[');
		for (int i = 0; i < mFractions.length; i++) {

			for (int j = 0; j < mFractions[i].length; j++) {
				if (j == 0) {
					sb.append('[');
				}
				sb.append(mFractions[i][j]).append(',');
				if (j == mFractions[i].length - 1) {
					sb.append(']');
					if (i != mFractions.length - 1) {
						sb.append(',');
					}
				}
			}
			if (i == mFractions.length - 1) {
				sb.append(']');
			}
		}
		return sb.toString();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (Fraction[] mFraction : mFractions) {
			for (Fraction aMFraction : mFraction) {
				sb.append(aMFraction).append('\t');
			}
			sb.append(System.lineSeparator());
		}
		return sb.toString();
	}

	private Matrix calc(boolean pAddToBounce) {
		Matrix matrix = new Matrix(mFractions.length, mFractions[0].length);
		for (int i = 0; i < mFractions.length; i++) {
			for (int j = 0; j < mFractions[i].length; j++) {
				matrix.addCell(i, j, mFractions[i][j].calc(pAddToBounce));
			}
		}
		return matrix;
	}

}
