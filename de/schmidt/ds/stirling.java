package de.schmidt.ds;

/**
 * Created by Marc Schmidt on 19.01.2017.
 */
public class stirling {

	private static final String LINE_SEP = System.lineSeparator();
	private static final char TAB = '\t';

	/**
	 * Dimension der Tabelle
	 */
	private static final long DIM = 7;
	/**
	 * Anzeige der Ersten/Zweiten Art
	 */
	private static final boolean ERSTER_ART = false;
	private static final boolean ZWEITER_ART = true;

	public static void main(String[] args) {
		//		System.out.println(stirlingOne(7, 2));
		//		System.out.println(stirlingOne(7, -2));
		//		System.out.println(stirlingOne(-7, 2));
		//		System.out.println(stirlingOne(-7, -2));
		//		System.out.println(stirlingOne(2, 7));
		//		System.out.println(stirlingOne(2, -7));
		//		System.out.println(stirlingOne(-2, 7));
		//		System.out.println(stirlingOne(-2, -7));
		//		prlongTable(ZWEITER_ART);

//		int all = 0;
//		for (int i = 1; i <= 7; i++) {
//			System.out.println("{" + 7 + ',' + i + "} " + stirlingTwo(7, i));
//			all += stirlingTwo(7, i);
//		}
//		System.out.println("all = " + all);
//		System.out.println(stirlingTwo(29, 10));
		System.out.println(Long.MAX_VALUE);
		System.out.println(stirlingTwo(5,3));
	}

	/**
	 * Berechnet die Tabelle Stirlingzahlen {n k}, also der Stirlingzahlen 2. Art
	 */
	private static void prlongTable(boolean pErsterArt) {
		StringBuilder sb = new StringBuilder().append(line(DIM));
		for (long n = -DIM; n <= DIM; n++) {
			sb.append(LINE_SEP).append(n);
			for (long k = -DIM; k <= DIM; k++) {
				sb.append(TAB).append(pErsterArt ? stirlingOne(n, k) : stirlingTwo(n, k));
			}
		}
		System.out.println(sb);
	}

	private static StringBuilder line(long pDim) {
		StringBuilder sb = new StringBuilder().append("k:").append(TAB);
		for (long k = -pDim; k <= pDim; k++) {
			sb.append(k).append(TAB);
		}
		return sb;
	}

	private static long stirlingOne(long n, long k) {
		return stirlingTwo(-k, -n);
	}

	private static long stirlingTwo(long n, long k) {
		if (n == 0 && k == 0) {
			return 1;
		} else if (n > 0 && k == 0 || n < 0 && k == 0 || n == 0 && k > 0) {
			return 0;
		} else if (n > 0 && k > 0) {
			return k * stirlingTwo(n - 1, k) + stirlingTwo(n - 1, k - 1);
		} else if (n < 0 && k > 0) {
			return (stirlingTwo(n + 1, k) - stirlingTwo(n, k - 1)) / k;
		} else if (k < 0) {
			return stirlingTwo(n + 1, k + 1) - (k + 1) * stirlingTwo(n, k + 1);
		} else {
			System.err.println("Fehler");
			System.exit(0);
			return 0;
		}
	}
}
