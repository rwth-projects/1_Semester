package de.schmidt.task6_1;

public class Boxing {

	private Integer i1;
	private int i2;
	private Double d;
	private float f;

	public Boxing(int a1, int b1, int c1, int d1) {
		this.i1 = a1;
		this.i2 = b1;
		this.d = (double) d1;
		this.f = c1;
	}

	public Boxing(Integer a, int b, Double c, float d) {
		this.i1 = a;
		this.i2 = b;
		this.d = c;
		this.f = d;
	}

	public int f(int a, float y) { return 11; }

	public int f(Double b, long y) { return 12; }

	public Integer f(double c, int y) { return 13; }

	public double g(Float d) { return 7.0; }

	public Float g(double e) { return 8f; }

	public static void main(String[] args) {
		Boxing b1 = new Boxing(1, 2, 3, 4);
		System.out.println(b1.d);
		System.out.println(b1.f(7d, 8L));
		System.out.println(b1.f(10d, 17));
		System.out.println(b1.f(5, 6L));
		Boxing b2 = new Boxing(b1.i1, 5, 6, 9);
		System.out.println(b2.f);
		System.out.println(b2.f(b1.f, b1.i2));
		Boxing b3 = new Boxing(b2.i1, 14, 1.5, 16);
		System.out.println(b3.d);
		System.out.println(b3.g(b1.i1));
		System.out.println(b3.g(new Float(18)));
		System.out.println(b3.f(b2.g(19f), 21));
	}
}