package de.schmidt.task6_1;

/**
 * Created by Marc Schmidt on 01.12.2016.
 */
public class SearchArray {

	private static int sCallsNormal;
	private static int sCallsSorted;

	public static void main(String[] args) {
		int value = 6;
		int[] arr = {0, 1, 2, 2, 2, 3, 4, 5, 6, 7, 7, 8, 9};
		System.out.println(containsNormal(value, arr, 0));
		System.out.println(containsSorted(value, arr, 0, arr.length / 2));
		System.out.println("sCallsNormal = " + sCallsNormal);
		System.out.println("sCallsSorted = " + sCallsSorted);
	}

	private static boolean containsNormal(int pValue, int[] pArr, int pIndex) {
		sCallsNormal++;
		if (pValue == pArr[pIndex]) {
			return true;
		} else if (pIndex < pArr.length - 1 && containsNormal(pValue, pArr, ++pIndex)) {
			return true;
		} else {
			return false;
		}
	}

	private static boolean containsSorted(int pValue, int[] pArr, int pIndex, int pIndexSize) {
		sCallsSorted++;
		if (pIndex >= pArr.length) {
			return false;
		}
		if (pValue == pArr[pIndex]) {
			return true;
		} else {
			if (pIndexSize > 1) {
				if (pValue > pArr[pIndex]) {
					pIndex += pIndexSize;
				} else {
					pIndex -= pIndexSize;
				}
			} else {
				if (pValue > pArr[pIndex]) {
					pIndex++;
				} else {
					pIndex--;
				}
			}
			return containsSorted(pValue, pArr, pIndex, pIndexSize / 2);
		}
	}

}
