package de.schmidt.task5_1;

public class fg {

	public static void main(String[] args) {
		Wrapper w1 = new Wrapper();
		Wrapper w2 = new Wrapper();
		w1.i = 1;
		w2.i = 2;
		int x = 3;
		int[] a = {4, 5};
		f(false, w2, w1, a);
		f(true, w1, w2, a[0], x);
//		w1 = new Wrapper();
//		w1.i = 6;
//		f(true, w1, w2, new int[] {a[0], a[1]});
		System.out.println();
		System.out.println("main:");
		System.out.println(a);
		arr(a);
		System.out.println("w1: " + w1 + "\nw2: " + w2 + "\nx: " + x);
	}

	public static void f(boolean show, Wrapper x, Wrapper y, int... a) {
		a[0] = y.i;
		y = x;
		y.i = a[1]; //Speicherzustand hier zeichnen }
		if (show) {
			System.out.println("f:");
			System.out.println("x: " + x + "\ny:" + y);
			System.out.println(a);
			arr(a);
		}
	}

	private static void arr(int... a){
		for (int i = 0; i < a.length; i++) {
			System.out.println(i + ", " + a[i]);
		}
	}
}