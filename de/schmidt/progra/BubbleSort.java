package de.schmidt.progra;

public class BubbleSort {

	public static void main(String[] args) {
		bubble(new int[] {3, 2, 1});
	}

	public static void bubble(int[] a) {
		for (int i = 1; i < a.length; i++) {
			for (int j = 0; j < a.length - i; j++) {
				if (a[j] > a[j + 1]) {
					int tmp = a[j];
					a[j] = a[j + 1];
					a[j + 1] = tmp;
				}
			}
		}
	}
}
