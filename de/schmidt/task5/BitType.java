package de.schmidt.task5;

/**
 * Beinhaltet verschiedene Bohrer
 */
public enum BitType {

	Twist, Dowelling, Masonry;

	/**
	 * Gibt alle Materialien zurueck, mit denen die Bohrer arbeiten koennen.
	 *
	 * @param pType Bohrer fuer den die Materialien herausgesucht werden
	 * @return alle Materialien mit den der Bohrer arbeiten kann.
	 */
	public static Material[] canHandle(BitType pType) {
		switch (pType) {
			case Twist:
				return new Material[] {Material.Wood, Material.Plastic, Material.Metal};
			case Dowelling:
				return new Material[] {Material.Wood, Material.Plastic};
			case Masonry:
				return new Material[] {Material.Stone, Material.Concrete, Material.ReinforcedConcrete};
			default:
				return null;
		}
	}
}