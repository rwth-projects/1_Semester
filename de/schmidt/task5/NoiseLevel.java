package de.schmidt.task5;

/**
 * Verwaltet ein Geraeuschniveau, dass als Ganzzahl dargestellt wird und erhoeht werden kann.
 */
public class NoiseLevel {

	/**
	 * Wird im Folgendem als {@code aktuelles Geraeuschniveau} bezeichnet
	 */
	private int mNoiseLevel;


	/**
	 * Erstellt ein {@code NoiseLevel} Objekt mit dem Standard Gerauschelevel 0
	 */
	public NoiseLevel() {
		mNoiseLevel = 0;
	}

	/**
	 * Aktualisiert das Geraeuschelevel, nach dem folgendem Prinzip:
	 * Wenn das uebergebene und aktuelle Gerauschelevel groesser als 10 sind, wird das aktuelle Geraeuschelevel auf den maximalen Wert plus 1
	 * gesetzt.
	 * Ansonsten wird das aktuelle Geraeuschniveau auf den maximalen Wert gesetzt.
	 * Der maximale Wert berechnet sich aus dem uebergebenen und aktuellen Geraeuschniveau {@link NoiseLevel#mNoiseLevel}, die der Methode
	 * {@link NoiseLevel#max(int, int)} uebergeben werden.
	 *
	 * @param pNoiseLevel uebergebenes Geraueschlevel
	 */
	public void add(int pNoiseLevel) {
		if (mNoiseLevel >= 10 && pNoiseLevel >= 10) {
			mNoiseLevel = max(mNoiseLevel, pNoiseLevel) + 1;
		} else {
			mNoiseLevel = max(mNoiseLevel, pNoiseLevel);
		}
	}

	/**
	 * Gibt das maximale Geraeuschniveau zurueck.
	 *
	 * @param pLevel1 erstes Geraeuschniveau, dass verglichen wird
	 * @param pLevel2 zweites Geraeschlevel, dass verglichen wird
	 * @return maximales Geraeuschelevel
	 */
	private int max(int pLevel1, int pLevel2) {
		return pLevel1 > pLevel2 ? pLevel1 : pLevel2;
	}

	/**
	 * @return Verschiedene Texte anhand des Geraeuschniveaus {@link NoiseLevel#mNoiseLevel}.
	 */
	public String toString() {
		if (mNoiseLevel < 2) {
			return "leise";
		} else if (mNoiseLevel < 5) {
			return "normal";
		} else if (mNoiseLevel < 10) {
			return "laut";
		} else if (mNoiseLevel < 12) {
			return "LAUT";
		} else {
			return "furchtbar laut";
		}
	}

	/**
	 * @return das aktuelle Geraeuschniveau {@link NoiseLevel#mNoiseLevel} zurueck
	 */
	public int getNoiseLevel() {
		return mNoiseLevel;
	}

	/**
	 * Setzte das aktuelle Geraeuschniveau auf den uebergeben Wert.
	 *
	 * @param pNoiseLevel Wert auf den das aktuelle Gerauschlevel gesetzt.
	 */
	public void setNoiseLevel(int pNoiseLevel) {
		mNoiseLevel = pNoiseLevel;
	}

}
