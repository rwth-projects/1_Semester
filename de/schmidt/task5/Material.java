package de.schmidt.task5;

/**
 * Beinhaltet verschiedene Typen von Materialien
 */
public enum Material {

	Wood, Plastic, Metal, Stone, Concrete, ReinforcedConcrete

}
