package de.schmidt.task6;

import java.util.Arrays;

/**
 * Klasse, welche die Schnittmenge der Eintraege von zwei Arrays berechnet.
 */
public class ArrayIntersect {

	static int sCalls = 0;
	static int sCallsSorted = 0;

	public static int[] arrayIntersection(int[] a, int[] b) {
		return buildIntersection(a, 0, b, new int[0]);
	}

	/**
	 * Erstellt ein Array mit allen Elementen, die in beiden uebergebenen Arrays mindestens einmal enthalten sind.
	 *
	 * @param pArr1      1. Array
	 * @param pIndex     Index des 1. Arrays
	 * @param pArr2      2. Array
	 * @param pIntersect Array das alle Elemente enthaelt die Elemente des 1. Arrays bis zum Index {@code pIndex} und im 2. Array enthalten sind.
	 * @return siehe Beschreibung zu {@code pIntersect}
	 */
	private static int[] buildIntersection(int[] pArr1, int pIndex, int[] pArr2, int[] pIntersect) {
		sCalls++;
		if (contains(pArr1[pIndex], pArr2, 0)) {
			int[] tmp = Arrays.copyOf(pIntersect, pIntersect.length + 1);
			tmp[pIntersect.length] = pArr1[pIndex];
			pIntersect = tmp;
		}

		if (pIndex < pArr1.length - 1) {
			return buildIntersection(pArr1, ++pIndex, pArr2, pIntersect);
		} else {
			return pIntersect;
		}
	}

	/**
	 * Gibt zurueck, ob das uebergebene Array den uebergebenen Wert enthaelt.
	 *
	 * @param pValue Wert dessen Existenz ueberprueft wird.
	 * @param pArr   Array in dem nach dem nach {@code pArr} gesucht wird.
	 * @param pIndex Index des aktuellen Elements aus {@code pArr}, dass verglichen wird.
	 * @return {@code true} wenn das Array den Wert enthaelt, sonst {@code false}.
	 */
	private static boolean contains(int pValue, int[] pArr, int pIndex) {
		sCalls++;
		return pValue == pArr[pIndex] || (pIndex < pArr.length - 1 && contains(pValue, pArr, ++pIndex));
	}


	public static int[] sortedArrayIntersection(int[] a, int[] b) {
		return buildSortedIntersection(a, 0, b, new int[0]);
	}

	/**
	 * Erstellt ein Array mit allen Elementen, die beiden uebergebenen Arrays mindestens einmal enthalten sind. Falls Elemente in Arrays mehrmals
	 * enthalten sind, werden sie dennoch nur einmal in das zurueckgegebene Array gepackt.
	 *
	 * @param pArr1      1. Array, aufsteigend sortiert
	 * @param pIndex     Index des 1. Arrays
	 * @param pArr2      2. Array, aufsteigend sortiert
	 * @param pIntersect Array das alle Elemente enthaelt die Elemente des 1. Arrays bis zum Index {@code pIndex} und im 2. Array enthalten sind,
	 *                   ohne Duplikate.
	 * @return siehe Beschreibung zu {@code pIntersect}
	 */
	private static int[] buildSortedIntersection(int[] pArr1, int pIndex, int[] pArr2, int[] pIntersect) {
		sCallsSorted++;
		if ((pIntersect.length == 0 || pArr1[pIndex] != pIntersect[pIntersect.length - 1]) && containsSorted(pArr1[pIndex], pArr2, pArr2.length / 2,
				pArr2.length / 4, false)) {
			int[] tmp = Arrays.copyOf(pIntersect, pIntersect.length + 1);
			tmp[pIntersect.length] = pArr1[pIndex];
			pIntersect = tmp;
		}

		if (pIndex < pArr1.length - 1) {
			return buildSortedIntersection(pArr1, ++pIndex, pArr2, pIntersect);
		} else {
			return pIntersect;
		}
	}

	/**
	 * Gibt zurueck, ob das uebergebene Array {@code pArr} den uebergebenen Wert {@code pValue} enthaelt. Es wird ein aufsteigend sortiertes Array
	 * durchsucht. Dabei wird der moegliche Spielraum in dem der gesucht Wert seien kann mit jedem rekursiven Aufruf halbiert, bis er kleiner gleich
	 * 1 wird. Dann wird nur noch ein weiteres Element verglichen.
	 *
	 * @param pValue     Wert der gesucht wird
	 * @param pArr       aufsteigend sortiertes Array in dem gesucht wird
	 * @param pIndex     Index der aktuellen Suche
	 * @param pIndexSize Wert der sich bei jeden rekursiven Aufruf halbiert (es wird abgerundet).
	 * @return {@code true} wenn das Array den Wert enthaelt, sonst {@code false}.
	 */
	private static boolean containsSorted(int pValue, int[] pArr, double pIndex, double pIndexSize, boolean pLarger) {
		sCallsSorted++;
		if (pValue == pArr[(int) pIndex]) {
			return true;
		} else if (pIndexSize < 0 && pLarger != pValue > pArr[(int) pIndex]) {
			return false;
		} else {
			pLarger = pValue > pArr[(int) pIndex];
			if (pIndexSize > 1) {
				if (pValue > pArr[(int) pIndex]) {
					pIndex += pIndexSize;
				} else {
					pIndex -= pIndexSize;
				}
			} else {
				if (pValue > pArr[(int) pIndex]) {
					pIndex++;
				} else {
					pIndex--;
				}
			}
			double index = pIndexSize <= 1 ? --pIndexSize : pIndexSize / 2;

			if (pIndexSize < 0 && pLarger != pValue > pArr[(int) pIndex]) {
				return pValue == pArr[(int) pIndex];
			}
			return pIndex >= 0 && pIndex < pArr.length && containsSorted(pValue, pArr, pIndex, index, pLarger);
		}
	}
}

