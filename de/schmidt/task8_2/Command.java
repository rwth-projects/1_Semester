package de.schmidt.task8_2;

public class Command {

	private VCS mVCS;

	public Command(VCS pVCS) {
		mVCS = pVCS;
	}

	public void execute() {
		// override me!
	}

	public static Command parse(String cmdName, VCS vcs) {
		if (cmdName.equals("listfiles")) {
			return new ListFiles(vcs);
		} else if (cmdName.equals("commit")) {
			return new Commit(vcs);
		} else if (cmdName.equals("exit")) {
			return new Exit(vcs);
		}
		System.err.println(cmdName + " ist kein gültiges Kommando");
		return null;
	}

	public VCS getVCS() {
		return mVCS;
	}
}
