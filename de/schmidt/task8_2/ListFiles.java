package de.schmidt.task8_2;

/**
 * Created by Marc Schmidt on 16.12.2016.
 */
public class ListFiles extends Command {

	public ListFiles(VCS pVCS) {
		super(pVCS);
	}

	/**
	 * Gibt ie Namen aller Dateien im Wurzelverzeichnis aus.
	 */
	public void execute(){
		String[] names = Util.listFiles(getVCS().getRootDir());
		for (String name : names) {
			System.out.println(name);
		}
	}
}
