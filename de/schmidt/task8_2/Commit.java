package de.schmidt.task8_2;

import java.io.File;

/**
 * Created by Marc Schmidt on 16.12.2016.
 */
public class Commit extends ListFiles {

	public Commit(VCS pVCS) {
		super(pVCS);
	}

	public void execute() {
		String newDir = Util.appendFileOrDirname(getVCS().getBackupDir(), Util.getTimestamp());
		System.out.println("newDir = " + newDir);
		Util.mkdir(newDir);


		String[] files = Util.listFiles(getVCS().getBackupDir());
		for (String file : files) {
			System.out.println("file: " + file);
			Util.moveFile(getVCS().getBackupDir() + File.separator + file, newDir + File.separator + file);
		}

		files = Util.listFiles(getVCS().getRootDir());
		for (String file : files) {
			System.out.println("file: " + file);
			Util.copyFile(getVCS().getRootDir() + File.separator + file, newDir + File.separator + file);
		}

		System.out.println("Comitted the following files:");
		super.execute();
	}
}