package de.schmidt.task8_2;

/**
 * Created by Marc Schmidt on 16.12.2016.
 */
public class Exit extends Command {

	public Exit(VCS pVCS) {
		super(pVCS);
	}

	/**
	 * Beendet die Anwendung
	 */
	public void execute(){
		Util.exit();
	}
}
