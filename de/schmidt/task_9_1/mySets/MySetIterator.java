package de.schmidt.task_9_1.mySets;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Created by Marc Schmidt on 22.12.2016.
 */
class MySetIterator<T> implements Iterator<T> {

	private MySetElement<T> current;

	MySetIterator(MySetElement<T> pElement) {
		current = pElement;
	}

	@Override
	public boolean hasNext() {
		return current != null;
	}

	@Override
	public T next() {
		if (current == null) {
			throw new NoSuchElementException("kein naechstes Element existiert");
		} else {
			T t = (T) current;
			current = current.getNext();
			return t;
		}
	}

	MySetElement<T> getCurrent() {
		return current;
	}
}
