package de.schmidt.task_9_1.mySets;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by Marc Schmidt on 22.12.2016.
 */
public class MyMutableSet<T> extends MyAbstractSet<T> implements Set {

	public MyMutableSet() {
		super(null);
	}

	@Override
	public int size() {
		int size = 0;
		Iterator<T> iter = iterator();
		while (iter.hasNext()) {
			size++;
		}
		return size;
	}

	@Override
	public boolean add(Object pO) {
		if (contains(pO)) {
			return false;
		} else {
			setHead(new MySetIterator<>(new MySetElement<>(getHead().getCurrent(), (T) pO)));
			return true;
		}
	}

	@Override
	public boolean remove(Object o) {
		MySetIterator<T> iter = getHead();
		MySetElement<T> elementLast = iter.getCurrent();
		MySetElement<T> element;
		while (iter.hasNext()) {
			element = iter.getCurrent();
			if (element.equals(o)) {
				iter.next();
				if (element == elementLast) {
					setHead(new MySetIterator<>(iter.getCurrent()));
				} else {
					elementLast.setNext(iter.getCurrent());
					setHead(new MySetIterator<>(elementLast));
				}
				return true;
			}
			elementLast = element;
			iter.next();
		}
		return false;
	}

	@Override
	public boolean addAll(Collection c) {
		if(c.isEmpty()){
			return true;
		}

		Iterator<T> iter = c.iterator();
		while (iter.hasNext()){
			add(iter.next());
		}
		return true;
	}

	@Override
	public void clear() {
		setHead(new MySetIterator<T>(null));
	}

	@Override
	public boolean removeAll(Collection c) {
		if(c.isEmpty()){
			return true;
		}

		Iterator<T> iter = c.iterator();
		while (iter.hasNext()){
			remove(iter.next());
		}
		return true;
	}

	@Override
	public boolean retainAll(Collection c) {
		throw new UnsupportedOperationException("wird nicht implementiert");
	}

	public MyMinimalSet<T> freezeAndClear(){
		MyImmutableSet <T> set = new MyImmutableSet<T>(getHead());
		setHead(null);
		return set;
	}
}
