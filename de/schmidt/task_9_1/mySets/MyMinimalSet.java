package de.schmidt.task_9_1.mySets;

import java.util.Collection;

/**
 * Created by Marc Schmidt on 22.12.2016.
 */
public interface MyMinimalSet<T> extends Iterable {

	/**
	 * Es soll eine Methode anbieten, um zu ueberpruefen, ob ein gegebenes Element Teil der Menge ist
	 *
	 * @param pO Objekt, dass ueberprueft wird
	 * @return {@code true} wenn das Objekt bereits enthalten ist, sonst {@code false}
	 */
	boolean contains(Object pO);

	/**
	 * Es soll eine Methode void addAllTo(Collection<T> col) zur Verfügung stellen, die alle Elemente des MyMinimalSets zu der als Argument
	 * uebergebenen Collection hinzufuegt
	 *
	 * @param col
	 */
	void addAllTo(Collection<T> col) throws UnmodifiableCollectionException;

}
