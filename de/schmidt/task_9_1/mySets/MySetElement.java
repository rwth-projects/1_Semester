package de.schmidt.task_9_1.mySets;

/**
 * Created by Marc Schmidt on 22.12.2016.
 */
class MySetElement<T> {

	private MySetElement<T> next;
	private T value;

	MySetElement(MySetElement<T> pNext, T pValue) {
		next = pNext;
		value = pValue;
	}

	public MySetElement<T> getNext() {
		return next;
	}

	public T getValue() {
		return value;
	}

	public void setNext(MySetElement<T> pNext) {
		next = pNext;
	}

	public void setValue(T pValue) {
		value = pValue;
	}
}
