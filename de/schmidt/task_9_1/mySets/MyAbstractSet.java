package de.schmidt.task_9_1.mySets;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by Marc Schmidt on 22.12.2016.
 */
abstract class MyAbstractSet<T> implements Iterable, Set {

	private MySetIterator<T> head;

	public MyAbstractSet(MySetIterator pIter) {
		head = pIter;
	}

	@Override
	public boolean isEmpty() {
		return head != null;
	}

	@Override
	public boolean contains(Object o) {
		while (head.hasNext()) {
			if (head.next().equals(o)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean containsAll(Collection c) {
		if(c.isEmpty()){
			return true;
		}

		Iterator<T> iter = c.iterator();
		while (iter.hasNext()){
			if(!contains(iter.next())){
				return false;
			}
		}
		return true;
	}

	@Override
	public Object[] toArray() {
		throw new UnsupportedOperationException("toArray wird nicht implementiert");
	}

	@Override
	public T[] toArray(Object[] a) {
		throw new UnsupportedOperationException("toArray wird nicht implementiert");
	}

	@Override
	public Iterator<T> iterator() {
		return head;
	}

	public MySetIterator<T> getHead() {
		return head;
	}

	public void setHead(MySetIterator<T> pHead) {
		head = pHead;
	}
}
