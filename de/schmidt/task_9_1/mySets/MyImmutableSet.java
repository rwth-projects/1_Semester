package de.schmidt.task_9_1.mySets;

import java.util.Collection;
import java.util.Iterator;

/**
 * Alle Methoden der Oberklasse MyAbstractSet und MyMinimalSet, die ueberschrieben werden muessen und das Ser veraendern werden nicht implementiert.
 * Created by Marc Schmidt on 22.12.2016.
 */
class MyImmutableSet<T> extends MyAbstractSet implements MyMinimalSet {

	MyImmutableSet(MySetIterator pIter) {
		super(pIter);
	}


	@Override
	public void addAllTo(Collection col) throws UnmodifiableCollectionException {
		try {

		} catch (UnsupportedOperationException pE) {
			throw new UnmodifiableCollectionException();
		}
	}

	@Override
	public int size() {
		int size = 0;
		Iterator<T> iter = iterator();
		while (iter.hasNext()) {
			size++;
		}
		return size;
	}

	@Override
	public boolean add(Object pO) {
		return false;
	}

	@Override
	public boolean remove(Object o) {
		return false;
	}

	@Override
	public boolean addAll(Collection c) {
		return false;
	}

	@Override
	public void clear() {

	}

	@Override
	public boolean removeAll(Collection c) {
		return false;
	}

	@Override
	public boolean retainAll(Collection c) {
		return false;
	}
}
