package de.schmidt.bruteforce;

/**
 * Created by Marc Schmidt on 10.11.2016.
 */
public class Minterm implements IBruteForce {

	private int[] mMintern;

	public Minterm(String pMinterm) {
		if (pMinterm.length() !=/* 5*/4) {
			System.err.println("Falscher Ausdruck");
		}

		mMintern = new int[pMinterm.length()];
		for (int i = 0; i < pMinterm.length(); i++) {
			mMintern[i] = pMinterm.charAt(i) == C_TRUE ? TRUE : FALSE;
		}
		System.out.println("p: " + this);
	}

	public Minterm(int... pPMinterm) {
		if (pPMinterm.length != /*5*/4) {
			System.err.println("Falscher Ausdruck");
			for (int i : pPMinterm) {
				System.out.println(i);
			}
		}

		mMintern = pPMinterm;
	}

	public boolean calc(int... pBool) {
		if (mMintern.length != pBool.length) {
			System.err.println("Falsche Laenge im Array");
		}
		for (int i = 0; i < mMintern.length; i++) {
			if (mMintern[i] != DONT_CARE && mMintern[i] != pBool[i]) {
				return false;
			}
		}
		return true;
	}

	@Override
	public String toString() {
		return "[" + mMintern[0] + ", " + mMintern[1] + ", " + mMintern[2] + ", " + mMintern[3] /*+ ", " + mMintern[4]*/ + "]";
	}
}
