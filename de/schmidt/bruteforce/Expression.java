package de.schmidt.bruteforce;

import java.util.ArrayList;

/**
 * Created by Marc Schmidt on 10.11.2016.
 */
public class Expression implements IBruteForce {

	private Minterm[] mMinterms;

	public Expression(ArrayList<Minterm> pMinterms) {
		mMinterms = new Minterm[pMinterms.size()];
		for (int i = 0; i < pMinterms.size(); i++) {
			mMinterms[i] = pMinterms.get(i);
		}
	}

	public boolean equals(Expression pExpr, String pComp) {
		int x0 = pComp.charAt(0) == C_TRUE ? TRUE : FALSE;
		int x1 = pComp.charAt(1) == C_TRUE ? TRUE : FALSE;
		int x2 = pComp.charAt(2) == C_TRUE ? TRUE : FALSE;
		int x3 = pComp.charAt(3) == C_TRUE ? TRUE : FALSE;
//		int x4 = pComp.charAt(3) == C_TRUE ? TRUE : FALSE;
		return calc(x0, x1, x2, x3/*, x4*/) == pExpr.calc(x0, x1, x2, x3/*, x4*/);
	}

	public boolean calc(int pX0, int pX1, int pX2, int pX3/*, int pX4*/) {
		for (Minterm minterm : mMinterms) {
			if (minterm.calc(pX0, pX1, pX2, pX3/*, pX4*/)) {
				return true;
			}
		}
		return false;
	}
}
