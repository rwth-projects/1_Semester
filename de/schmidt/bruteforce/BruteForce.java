package de.schmidt.bruteforce;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Marc Schmidt on 10.11.2016.
 */
public class BruteForce implements IBruteForce {

	private static Expression sExpressions;
	private static Minterm[] sMinterms;

	public static void main(String[] args) {
		///*4*/parseMinterms("x4¯x3¯x2x1¯x0¯;x3x2x1x0;x3x2¯x1x0;x4x3x2x1¯x0¯;x4¯x3¯x1x0;x4¯x3x2x1¯x0;x3x1¯x0;x2¯x1¯x0¯");
		/*5*/
		parseMinterms("x3¯x2¯x1¯x0¯;x3¯x2¯x1¯x0;x3¯x2¯x1x0¯;x3¯x2¯x1x0;x3¯x2x1¯x0¯;x3¯x2x1¯x0;x3¯x2x1x0¯;x3¯x2x1x0;x3x2¯x1¯x0¯;x3x2¯x1¯x0;" +
				"x3x2¯x1x0¯;x3x2¯x1x0;x3x2x1¯x0¯;x3x2x1¯x0;x3x2x1x0¯;x3x2x1x0;");
		for (Minterm minterm : sMinterms) {
			System.out.println("m: " + minterm);
		}
		///*4*/parseExpression("00011001000101000000000000011000");
		/*5*/
		parseExpression("0001001000000111");
		Set<Minterm> mySet = new HashSet<>(sMinterms.length);
		for (Minterm minterm : sMinterms) {
			mySet.add(minterm);
		}

		StringBuilder results = new StringBuilder("results: ");
		for (Set<Minterm> set : powerSet(mySet)) {
			ArrayList<Minterm> terms = new ArrayList<>();
			terms.addAll(set);

			Expression expr = new Expression(terms);
			boolean equals = true;
			for (String combination : COMBINATIONS) {
				if (!expr.equals(sExpressions, combination)) {
					equals = false;
					break;
				}
			}

			if (equals) {
				results.append(LINE_SEP);
				for (Minterm term : terms) {
					results.append(term);
				}
			}
		}

		System.out.println(results);
	}

	private static void parseMinterms(String pStr) {
		String[] parts = pStr.split(SPLIT);
		sMinterms = new Minterm[parts.length];
		for (int i = 0; i < sMinterms.length; i++) {
			sMinterms[i] = parseMinterm(parts[i]);
		}
	}

	private static Minterm parseMinterm(String pStr) {
		int[] parts = new int[/*5*/4];
		for (int i = 0; i < parts.length; i++) {
			int index = pStr.indexOf("x" + /*(4 - i)*/i);
			if (index == -1) {
				parts[i] = DONT_CARE;
			} else {
				if (index + 2 < pStr.length() && pStr.charAt(index + 2) == C_FALSE) {
					parts[i] = FALSE;
				} else {
					parts[i] = TRUE;
				}
			}
		}
		return new Minterm(parts);
	}

	private static void parseExpression(String pStr) {
		if (pStr.length() != 16) {
			System.err.println("zu wenig Ausdruecke");
		}
		ArrayList<Minterm> minterms = new ArrayList<>();
		for (int i = 0; i < pStr.length(); i++) {
			if (pStr.charAt(i) == C_TRUE) {
				minterms.add(new Minterm(COMBINATIONS[i]));
			}
		}

		sExpressions = new Expression(minterms);
	}

	public static <T> Set<Set<T>> powerSet(Set<T> originalSet) {
		Set<Set<T>> sets = new HashSet<Set<T>>();
		if (originalSet.isEmpty()) {
			sets.add(new HashSet<T>());
			return sets;
		}
		List<T> list = new ArrayList<T>(originalSet);
		T head = list.get(0);
		Set<T> rest = new HashSet<T>(list.subList(1, list.size()));
		for (Set<T> set : powerSet(rest)) {
			Set<T> newSet = new HashSet<T>();
			newSet.add(head);
			newSet.addAll(set);
			sets.add(newSet);
			sets.add(set);
		}
		return sets;
	}

}
