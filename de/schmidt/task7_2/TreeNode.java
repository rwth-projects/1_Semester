package de.schmidt.task7_2;

/**
 * Ein Konten in einem binaeren Baum.
 * <p>
 * Der gespeicherte Wert ist unveraenderlich,
 * die Referenzen auf die Nachfolger koennen aber
 * geaendert werden.
 * <p>
 * Die Klasse bietet Methoden, um Werte aus einem Baum
 * zu suchen, einzufuegen und zu loeschen. Diese gibt
 * es jeweils noch in optimierten Varianten, um
 * rotate-to-root Baeume zu verwalten.
 */
public class TreeNode {

	/**
	 * Linker Nachfolger
	 */
	private TreeNode left;
	/**
	 * Rechter Nachfolger
	 */
	private TreeNode right;
	/**
	 * Wert, der in diesem Knoten gespeichert ist
	 */
	private final int value;

	/**
	 * Erzeugt einen neuen Knoten ohne Nachfolger
	 */
	public TreeNode(int val) {
		this.value = val;
		this.left = null;
		this.right = null;
	}

	/**
	 * Erzeugt einen neuen Knoten mit den gegebenen Nachfolgern
	 */
	public TreeNode(int val, TreeNode left, TreeNode right) {
		this.value = val;
		this.left = left;
		this.right = right;
	}

	public int getValue() {
		return this.value;
	}

	/**
	 * Der gespeicherte Wert, umgewandelt in einen String
	 */
	public String getValueString() {
		return Integer.toString(this.value);
	}

	public boolean hasLeft() {
		return this.left != null;
	}

	public boolean hasRight() {
		return this.right != null;
	}

	public TreeNode getLeft() {
		return this.left;
	}

	public TreeNode getRight() {
		return this.right;
	}

	/**
	 * Sucht in diesem Teilbaum nach x, ohne den Baum zu veraendern. Wenn der gesuchte Wert nicht gefunden wurde und er kleiner als der der aktuelle
	 * ist und der linker Teilbaum existiert, wird in diesem weiter gesucht. Wenn der gesuchte Wert nicht gefunden wurde und er groesser als der der
	 * aktuelle ist und der rechte Teilbaum existiert, wird in diesem weiter gesucht. Andernfalls wird {@code false} zurueckgegeben.
	 *
	 * @return true, falls x enthalten ist, sonst false
	 */
	public boolean simpleSearch(int x) {
		if (x == value) {
			return true;
		} else if (x < value) {
			return hasLeft() && left.simpleSearch(x);
		} else if (x > value) {
			return hasRight() && right.simpleSearch(x);
		}
		return false;
	}

	/**
	 * Fuegt x in diesen Teilbaum ein, sofern kein Knoten mit dem Wert x existiert.
	 */
	public void insert(int x) {
		TreeNode node = search(x);
		if (node != null) {
			if (x < node.value) {
				node.left = new TreeNode(x);
			} else {
				node.right = new TreeNode(x);
			}
		}
	}

	/**
	 * Gibt {@code null} zurueck, wenn der Knoten oder einer seine Kind-Knoten den Wert {@code x} hat. Wenn der Wert nicht enthalten ist wird das
	 * aktuelle Objekt der Suche zurueckgegeben.
	 *
	 * @param x Wert der gesucht wird
	 * @return {@code null}, falls der Wert gefunden wurde, sonst das aktuelle Objekt der Suche.
	 */
	private TreeNode search(int x) {
		if (x == value) {
			return null;
		} else if (x < value) {
			if (hasLeft()) {
				return left.search(x);
			} else {
				return this;
			}
		} else {
			if (hasRight()) {
				return right.search(x);
			} else {
				return this;
			}
		}
	}

	/**
	 * Sucht in diesem Teilbaum nach x und rotiert den Endpunkt der Suche in die
	 * Wurzel.
	 *
	 * @param x der gesuchte Wert
	 * @return die neue Wurzel des Teilbaums
	 */

	public TreeNode rotationSearch(int x) {
		//TODO d)
		return null;
		//Ende TODO
	}

//	/**
//	 * Geordnete Liste aller Zahlen, die in diesem Teilbaum gespeichert sind.
//	 */
//	public String toString() {
//		//TODO c)
//		private void Arraysortieren(int[] zahl){
//        for (int letzter = zahl.length-1; letzter > 1; letzter--){
//            for(int aktuell = 0; aktuell < letzter; aktuell++){
//                if(zahl[aktuell] > zahl[aktuell + 1]){
//                    int temp = zahl[aktuell];
//                    zahl[aktuell] = zahl[aktuell+1];
//                    zahl[aktuell+1] = temp;
//                }
//            }
//
//        }
//return "TreeNode.toString(zahl)";
//    }
//
//
//    //Ende TODO
//
//	}

	/**
	 * Erzeugt eine dot Repraesentation in str
	 */
	public int toDot(StringBuilder str, int nullNodes) {
		if (this.hasLeft()) {
			str.append(this.getValueString() + " -> " + this.left.getValueString() + ";" + System.lineSeparator());
			nullNodes = this.left.toDot(str, nullNodes);
		} else {
			str.append("null" + nullNodes + "[shape=point]" + System.lineSeparator() + this.getValueString() + " -> null" + nullNodes + ";" + System
					.lineSeparator());
			nullNodes += 1;
		}
		if (this.hasRight()) {
			str.append(this.getValueString() + " -> " + this.right.getValueString() + ";" + System.lineSeparator());
			nullNodes = this.right.toDot(str, nullNodes);
		} else {
			str.append("null" + nullNodes + "[shape=point]" + System.lineSeparator() + this.getValueString() + " -> null" + nullNodes + ";" + System
					.lineSeparator());
			nullNodes += 1;
		}
		return nullNodes;
	}

}
