package de.schmidt.rb;//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

public class Room {
	private char[][] map = (char[][])null;
	int maxX;
	int maxY;

	public Room() {
	}

	public void setMap(String[] var1) {
		int var2 = 0;
		this.map = new char[var1.length][];
		String[] var3 = var1;
		int var4 = var1.length;

		for(int var5 = 0; var5 < var4; ++var5) {
			String var6 = var3[var5];
			if(var2 == 0) {
				this.maxX = var6.length();
			}

			assert this.maxX == var6.length();

			this.map[var2++] = var6.toCharArray();
		}

		this.maxY = var2;
	}

	public void runRB(RB var1) {
		InternalRB var2 = new InternalRB();
		this.render(var2);
		int var3 = 0;
		String var4 = "move";

		while(!var4.equals("end")) {
			byte var6 = -1;
			switch(var4.hashCode()) {
				case -988477796:
					if(var4.equals("pickUp")) {
						var6 = 3;
					}
					break;
				case -136570977:
					if(var4.equals("turnRight")) {
						var6 = 2;
					}
					break;
				case 3357649:
					if(var4.equals("move")) {
						var6 = 0;
					}
					break;
				case 133959204:
					if(var4.equals("turnLeft")) {
						var6 = 1;
					}
			}

			switch(var6) {
				case 0:
					var2.pos.add(var2.fwd);
					break;
				case 1:
					var2.fwd.rotate90(false);
					break;
				case 2:
					var2.fwd.rotate90(true);
					break;
				case 3:
					if(this.map[var2.pos.y][var2.pos.x] == 45) {
						this.map[var2.pos.y][var2.pos.x] = 32;
						++var3;
					}
			}

			this.render(var2);
			System.out.print("Energiezellen: ");
			System.out.print(var3);
			System.out.println();
			System.out.println();
			if(this.map[var2.pos.y][var2.pos.x] == 64) {
				System.out.println("Ausgang erreicht! Wuff!");
				return;
			}

			System.out.print("Neues Kommando: ");
			var4 = var1.schritt();
			System.out.println(var4);
		}

		System.out.println("Programm Ende erreicht. Und nun?");
	}

	private void render(InternalRB var1) {
		System.out.println();

		for(int var2 = 0; var2 < this.maxY; ++var2) {
			for(int var3 = 0; var3 < this.maxX; ++var3) {
				if(var1.pos.equal(var3, var2)) {
					System.out.print(var1.fwd.toArrow());
				} else {
					System.out.print(this.map[var2][var3]);
				}
			}

			System.out.println();
		}

		System.out.flush();
	}

	private class InternalRB {
		public vec2d pos = Room.this.new vec2d(1, 1);
		public vec2d fwd = Room.this.new vec2d(1, 0);

		public InternalRB() {
		}
	}

	private class vec2d {
		int x;
		int y;

		public vec2d(int var2, int var3) {
			this.x = var2;
			this.y = var3;
		}

		public void add(vec2d var1) {
			this.x += var1.x;
			this.y += var1.y;
		}

		public void rotate90(boolean var1) {
			int var2;
			if(var1) {
				var2 = this.x;
				this.x = -this.y;
				this.y = var2;
			} else {
				var2 = this.x;
				this.x = this.y;
				this.y = -var2;
			}

		}

		public boolean equal(int var1, int var2) {
			return var1 == this.x && var2 == this.y;
		}

		public char toArrow() {
			return (char)(this.x > 0?'>':(this.x < 0?'<':(this.y < 0?'^':'v')));
		}
	}
}
