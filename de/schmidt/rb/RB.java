package de.schmidt.rb;

public class RB {

	String[] mProgramMemory;
	int maxKmd;
	int mProgramPos;
	int[] mRegister;
	String[] TERMINALE = {"move", "turnLeft", "turnRight", "pickUp", "for", "endfor"};
	int[] MAX_TERMINALE = {1, 1, 1, 1, 3, 2};
	int INDEX_KOMMANDOS = 3;

	public RB(int pMaxCommands) {
		maxKmd = pMaxCommands;
	}

	public int programmHochladen(String[] a) {
		mRegister = new int[2];
		mProgramPos = 0;
		int tempKmd = 0;
		for (String i : a) {
			String[] splitted = i.split(" ");
			if (splitted[0].equals("move") || splitted[0].equals("turnLeft") || splitted[0].equals("turnRight") || splitted[0].equals("pickUp")) {
				if (splitted.length != 1) {
					return -1;
				}
				tempKmd++;
				if (tempKmd > maxKmd) {
					return -2;
				}
			} else if (splitted[0].equals("for")) {
				if (splitted.length != 3) {
					return -1;
				}
			} else if (splitted[0].equals("endfor")) {
				if (splitted.length != 2) {
					return -1;
				}
			}

		}

		mProgramMemory = a;

		return tempKmd;
	}

	public String schritt() {
		// In der Methode l�uft eine Schleife ab, bis das n�chste auszuf�hrende Kommando [...] gefunden wurde.
		while (true) {
			// In der Schleife wird zun�chst gepr�ft, ob die aktuelle Programmposition noch im Programm ist.


			if (mProgramPos >= mProgramMemory.length) {


				// Wenn das Programmende erreicht wurde, dann wird sofort end zur�ckgegeben.
				return "end";
			}


			// Ansonsten wird der String an der aktuellen Programmposition aus dem Programmspeicher gelesen.
			String[] parts = mProgramMemory[mProgramPos].split(" ");
			switch (parts[0]) {
				//  Ist es ein Kommando,...
				case "move":
				case "turnLeft":
				case "turnRight":
				case "pickUp":
					// ...wird die Programmposition um eins erh�ht und das Kommando zur�ckgegeben.
					mProgramPos++;
					return parts[0];
				// Bei einem for...
				case "for":
					// ...wird das Register gelesen, das durch die erste Zahl hinter dem for gegeben ist.
					int register = Integer.parseInt(parts[1]);
					// Anschlie�end wird der Wert im Register mit der zweiten Zahl hinter dem for verglichen.
					if (mRegister[register] < Integer.parseInt(parts[2])) {
						// Ist der Wert im Register kleiner, werden Programmposition und Wert im Register um eins erh�ht.
						mRegister[register]++;
					} else {
						//  Andernfalls wird die Programmposition so lange erh�ht, bis sie eins hinter dem n�chsten endfor steht, bei dem als Zahl
						// die Programmposition der for-Anweisung gegeben ist, die gerade ausgef�hrt wurde.
						int indexFor = mProgramPos;
						boolean run = true;
						while (run) {
							String[] tmp = mProgramMemory[mProgramPos].split(" ");
							if (tmp[0].equals("endfor")) {
								if (Integer.parseInt(tmp[1]) == indexFor) {
									run = false;
								} else {
									mProgramPos++;
								}
							} else {
								mProgramPos++;
							}
						}

						// Au�erdem wird das Register zur�ck auf 0 gesetzt.
						mRegister[register] = 0;
					}
					mProgramPos++;
					if (mProgramPos >= mProgramMemory.length) {
						return "end";
					}
					break;
				// Ist es ein endfor,...
				case "endfor":
					//...  wird die Programmposition auf die Zahl, die dem endfor folgt, gesetzt.
					mProgramPos = Integer.parseInt(parts[1]);
					break;
			}
		}
	}
}
