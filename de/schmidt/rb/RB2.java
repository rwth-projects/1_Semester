package de.schmidt.rb;

/**
 * Created by Marc Schmidt on 17.11.2016.
 */
public class RB2 {

	/**
	 * Erlaubte Terminale der Programmiersprache
	 */
	private static final String[] TERMINALE = {"move", "turnLeft", "turnRight", "pickUp", "for", "endfor"};
	/**
	 * Erlaubte Anzahl an Terminalen die in einer Zeile stehen duerfen. Die maximale Anzahl in einen Index von {@link RB2#MAX_TERMINALE} bezieht auf
	 * das Terminal mit den gleichen Index in {@link RB2#TERMINALE}
	 */
	private static final int[] MAX_TERMINALE = {1, 1, 1, 1, 3, 2};
	/**
	 * hoechster Index der Terminalen {@link RB2#TERMINALE}, die ein Kommando wie "move, pickUp..." darstellen
	 */
	private static final int INDEX_KOMMANDOS = 3;

	/**
	 * Den Programmspeicher, ein Array von Strings. Jeder String repraesentiert eine Zeile im Programm des RB2.
	 */
	private String[] mProgramMemory;

	/**
	 * Ein Array von int-Werten, als die Register des RB2.
	 */
	private int[] mRegister;

	/**
	 * Die maximale zulaessige Anzahl von Kommandos. Als "Kommando" bezeichnen wir hier die Bewegungsbefehle (move, turnLeft, turnRight, pickUp).
	 */
	private int mMaxCommands;

	/**
	 * Die aktuelle Position im Programm. Diese Variable sollte als Index fuer den Programmspeicher verwendet werden koennen.
	 */
	private int mProgramPos;

	public RB2(int pMaxCommands) {
		mMaxCommands = pMaxCommands;
	}

	public int programmHochladen(String[] pProgramm) {
		mRegister = new int[2];
		mProgramPos = 0;

		int tmpKommandos = 0;

		for (String str : pProgramm) {
			//Woerter einer Zeile
			String[] woerter = str.split(" ");
			for (int i = 0; i < woerter.length; i++) {
				//prueft, ob das erste Wort einer Zeile nach der Grammatik zulaessig ist.
				if (i == 0) {
					boolean isTerminal = false;
					for (int j = 0; j < TERMINALE.length; j++) {
						if (woerter[i].equals(TERMINALE[j])) {
							//gibt einen Fehler zurueck, wenn die Anzahl der Terminale nicht korrekt ist
							if (woerter.length != MAX_TERMINALE[j]) {
								return -1;
							}
							//erhoeht die Anzahl der genutzten Kommandos, wenn es "move, turnLeft, turnRight, pickUp ist, um eins und ueberprueft, ob
							// die Anzahl der maximalen Kommandos ueberschritten wurde. Gibt in diesem Fall -2 zurueck.
							else if (j < INDEX_KOMMANDOS) {
								tmpKommandos++;
								if (tmpKommandos > mMaxCommands) {
									return -2;
								}
							}
							isTerminal = true;
							break;
						}
					}

					//gibt einen Fehler zurueck, wenn das erste Wort einer Zeile nicht zur Grammatik gehoert
					if (!isTerminal) {
						return -1;
					}
				}
			}
		}

		//uebergebene Programm wird in den Programmspeicher des RBs abgelegt.
		mProgramMemory = pProgramm;

		//Anzahl der Kommandos
		return tmpKommandos;
	}

	public String schritt() {
		// In der Methode laeuft eine Schleife ab, bis das naechste auszufuehrende Kommando [...] gefunden wurde.
		while (true) {
			// In der Schleife wird zunaechst geprueft, ob die aktuelle Programmposition noch im Programm ist.
			if (mProgramPos >= mProgramMemory.length) {
				// Wenn das Programmende erreicht wurde, dann wird sofort end zurueckgegeben.
				return "end";
			}
			// Ansonsten wird der String an der aktuellen Programmposition aus dem Programmspeicher gelesen.
			String[] parts = mProgramMemory[mProgramPos].split(" ");
			switch (parts[0]) {
				//  Ist es ein Kommando,...
				case "move":
				case "turnLeft":
				case "turnRight":
				case "pickUp":
					// ...wird die Programmposition um eins erhoeht und das Kommando zurueckgegeben.
					mProgramPos++;
					return parts[0];
				// Bei einem for...
				case "for":
					// ...wird das Register gelesen, das durch die erste Zahl hinter dem for gegeben ist.
					int register = Integer.parseInt(parts[1]);
					// Anschliessend wird der Wert im Register mit der zweiten Zahl hinter dem for verglichen.
					if (mRegister[register] < Integer.parseInt(parts[2])) {
						// Ist der Wert im Register kleiner, werden Programmposition und Wert im Register um eins erhoeht.
						mRegister[register]++;
					} else {
						//  Andernfalls wird die Programmposition so lange erhoeht, bis sie eins hinter dem naechsten endfor steht, bei dem als Zahl
						// die Programmposition der for-Anweisung gegeben ist, die gerade ausgefuehrt wurde.
						int indexFor = mProgramPos;
						boolean run = true;
						while (run) {
							String[] tmp = mProgramMemory[mProgramPos].split(" ");
							if (tmp[0].equals("endfor")) {
								if (Integer.parseInt(tmp[1]) == indexFor) {
									run = false;
								} else {
									mProgramPos++;
								}
							} else {
								mProgramPos++;
							}
						}

						// Ausserdem wird das Register zurueck auf 0 gesetzt.
						mRegister[register] = 0;
					}
					mProgramPos++;
					if (mProgramPos >= mProgramMemory.length) {
						return "end";
					}
					break;
				// Ist es ein endfor,...
				case "endfor":
					//...  wird die Programmposition auf die Zahl, die dem endfor folgt, gesetzt.
					mProgramPos = Integer.parseInt(parts[1]);
					break;
			}
		}
	}
}
