package de.schmidt.klausur;

/**
 * Created by Marc Schmidt on 04.01.2017.
 */
public class B extends A {

	public static double y = 3;
	public double x = 0;

	public B(double x) { y++; }

	public void f(int y) {
		System.out.println("y = [" + y + "]");
		this.x = y * 2;
		B.y = 0;
	}

	public void f(double t) {
		System.out.println("t = [" + t + "]");
		this.x = 2 * t + B.y;
	}
}

