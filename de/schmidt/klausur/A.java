package de.schmidt.klausur;

import de.schmidt.task8_1.B;

/**
 * Created by Marc Schmidt on 04.01.2017.
 */
public class A {

	public int x = 2;

	public A() { this.x++; }

	public A(int x) {
		this.x += x;
	}

	public void f(int a){

	}

	public void f(double k) {
		System.out.println("k = [" + k + "]");
		this.x = (int) (k + B.y);
	}
}
